---
title: 命令行工具
slug: /
sidebar_position: 1
---

命令行工具([hustmirror-cli](https://gitee.com/dzm91_hust/hustmirror-cli.git))是一个可以帮助你快速换源的小工具。

![cli工具](/img/cli.svg)

```bash
curl -s https://hustmirror.cn/get | sh
```

**该命令行工具仅在同级目录下生成软件源配置文件。目前需要用户根据给出的命令自行替换配置文件。**

注：因为该工具处于测试阶段，因此并未进行软件源配置文件的替换操作。
