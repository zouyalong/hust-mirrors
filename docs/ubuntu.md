---
title: Ubuntu 镜像使用帮助
sidebar_label: Ubuntu
---
# Ubuntu

## 替换文件

本镜像仅包含32/64位 x86 架构处理器的软件包，
在ARM(arm64, armhf)、PowerPC(ppc64el)、RISC - V(riscv64) 和
S390x等架构的设备上（对应官方源为 ports.ubuntu.com）
请使用 `ubuntu-ports` 镜像。

Ubuntu 的软件源配置文件是 `/etc/apt/sources.list`。
为避免替换后出现问题，可以先将系统自带的该文件做个备份。
```bash varcode
[ ] (root) 是否为 root 用户
---
const SUDO = !root ? 'sudo ' : '';
---
${SUDO}cp /etc/apt/sources.list /etc/apt/sources.list.bak
```

然后将该文件替换为下面内容，即可使用选择的软件源镜像。

:::caution
因镜像站同步有延迟，可能会导致生产环境系统不能及时检查、安装上最新的安全更新，不建议替换 security 源。
:::


```bash varcode
[ ] (version) { jammy:22.04 LTS, lunar:23.04, kinetic:22.10, focal:20.04 LTS, bionic:18.04 LTS, xenial:16.04 LTS, trusty:14.04 LTS } Ubuntu 版本
[x] (secure) 使用官方安全更新软件源 
[ ] (proposed) 启用预发布软件源
[ ] (src) 启用源码镜像
---
const SRC_PREFIX = src ? "" : "# ";
const PROPOSED_PREFIX = proposed ? "" : "# ";
const SECURE_URL = secure ? '://security.ubuntu.com/ubuntu/' : `://${_domain}/ubuntu/`;
---
deb ${_http}://${_domain}/ubuntu/ ${version} main restricted universe multiverse
${SRC_PREFIX}deb-src ${_http}://${_domain}/ubuntu/ ${version} main restricted universe multiverse
deb ${_http}://${_domain}/ubuntu/ ${version}-updates main restricted universe multiverse
${SRC_PREFIX}deb-src ${_http}://${_domain}/ubuntu/ ${version}-updates main restricted universe multiverse
deb ${_http}${SECURE_URL} ${version}-security main restricted universe multiverse
${SRC_PREFIX}deb-src ${_http}${SECURE_URL} ${version}-security main restricted universe multiverse

${PROPOSED_PREFIX}deb ${_http}://${_domain}/ubuntu/ ${version}-proposed main restricted universe multiverse
${PROPOSED_PREFIX || SRC_PREFIX}deb-src ${_http}://${_domain}/ubuntu/ ${version}-proposed main restricted universe multiverse
```

保存该文件至`/etc/apt/sources.list`。


## 一键换源

:::caution
本方法仅适用于从官方源更换到本站源，如果你已经换过了源，请不要使用下面的命令。
:::

一般情况下，可以将 /etc/apt/sources.list 文件中 Ubuntu 默认的源地址 http://archive.ubuntu.com/ 使用sed命令直接替换。

可以使用如下命令：

```shell varcode
[ ] (root) 是否为 root 用户
---
const SUDO = !root ? 'sudo ' : '';
---
${SUDO}sed -i.bak 's|//.*archive.ubuntu.com|//${_domain}|g' /etc/apt/sources.list
```

本方法没有替换 security 源，如果想要替换 security 源可以执行以下命令：
```shell varcode
[ ] (root) 是否为 root 用户
---
const SUDO = !root ? 'sudo ' : '';
---
${SUDO}sed -i.bak 's/security.ubuntu.com/${_domain}/g' /etc/apt/sources.list
```

> 本方法借鉴了[中科大镜像源使用帮助](https://mirrors.ustc.edu.cn/help/ubuntu.html)
