const config = {
  title: "华中科技大学开源镜像站 | HUST Mirror",
  mainTitle: "华中科技大学开源镜像站",
  url: "https://hustmirror.cn",
  welcome: "欢迎来到华中科技大学开源镜像站，该站点由华中科技大学为你呈现。"
}

module.exports = config;
